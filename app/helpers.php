<?php

/**
 * Some global helper functions for the application
 */

/**
 * Gets and returns the current authenticated user
 * @return [type] [description]
 */
function current_user()
{
	return auth()->user();
}