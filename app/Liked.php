<?php

namespace App;
use App\Tweet;

trait Liked
{

 /**
 * Get the total amount of tweets the user has liked. 
 * @return [type] [description]
 * */
  public function likes()
  {
      return $this->hasMany(Like::class);
  }    

  /**
   * Caller for the total amount of user likes. 
   * @return [INT] This is the total number of tweets the user has liked. 
   */
  public function getUserLikesAttribute()
  {
      return $this->likes()->count();
  }

  /**
   *  Toggle method to see if a user has liked a tweet.
   * @param  [type] $tweet [description]
   * @return [type]        [description]
   */
  public function toggleLike($tweet)
  {
      // Like the tweet.
      if($this->likes()->where('tweet_id', $tweet->id)->count()===0)
      {
        return Like::create([
          'user_id'=>$this->id,
          'tweet_id'=>$tweet->id,
        ]);
      } 

      // Remove the tweet like
      else
      {
        $this->likes()
            ->where('tweet_id', $tweet->id)
                ->delete();
      }
  }
	
}