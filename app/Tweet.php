<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
	public $guarded = []; // Allow all, we'll sort on input. 

	/**
	 * Get the user the tweet belongs to.
	 * @return [obj] 
	 */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * Get the original author of the tweet.
     */
    public function getUsernameAttribute()
    {
        return $this::find($this->tweet_id)
            ->user
            ->username;
    }

    /**
     * Count the amount of times the tweet has been retweeted. 
     * @param  Tweet   $tweet These are the details of the tweet to be checked. 
     * @return [INT] Returns the number of times the tweet has been retweeted
     */
    public function isRetweeted(Tweet $tweet)
    {
        return Tweet::where('tweet_id',$tweet->id)
            ->where('rt',1)
            ->count();
    }

    /**
     * Get the likes for this post.
     * @return [type] [description]
     */
    public function likes()
    {
       return $this->hasMany(Like::class, 'tweet_id');
    }

    /**
     * Get the amount of likes for a particular tweet.
     * @return [type] [description]
     */
    public function getLikesCountAttribute()
    {
       return $this->likes->count();
    }
}
