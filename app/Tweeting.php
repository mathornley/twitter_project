<?php

namespace App;
use App\Tweet;

trait Tweeting
{
	/**
     * Get all the tweets from the user + who they follow. 
     * @return [obj] 
     */
    public function timeline()
    {
       // Get all the ids the user follows + add the user id also to the array. 
       $friends = $this->follows()
            ->pluck('id');
        //->push($this->id); // You could also push our ID on to the end of the array.

       // Get all the tweets using the ids, latest first, including ours as above.
       return Tweet::whereIn('user_id', $friends)
       ->orWhere('user_id', $this->id)
       ->latest()
       ->paginate(20);  
    }

    /**
     * Just get the users tweets exclusively.
     * @return [obj] 
     */
    public function tweets()
    {
       return $this->hasMany(Tweet::class)->latest();
    }

    /**
     * Get the total number of tweets the user has, inc retweets
     * @return [type] [description]
     */
    public function getTweetsCountAttribute()
    {
       return $this->tweets()
            ->count();
    }

    /**
     * Get the users retweet count
     * @return [type] [description]
     */
    public function getRetweetsCountAttribute()
    {
        return $this->tweets
            ->filter(function($me){return $me->rt==1;})
            ->count();
    }
}