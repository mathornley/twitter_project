<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * The first argument passed to these functions is always the current user first, then the object you're testing i.e user.
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can edit the profile using the UserPolicy.
     * The policy functions always uses the first argument as the current user.
     * Second argument is your user model you're testing.
     *
     * @param  \App\User  $current_user the currently signed in user.
     * @param  \App\User  $user the users profile.
     * @return mixed
     */
    public function edit(User $current_user, User $user)
    {
        // if the profile owner is the currently signed in user, return true.
        return $current_user->is($user);
    }

    /**
     * Determine whether the user can update the profile using the UserPolicy.
     * The policy functions always uses the first argument as the current user.
     * Second argument is your user model you're testing.
     *
     * @param  \App\User  $current_user the currently signed in user.
     * @param  \App\User  $user the users profile.
     * @return mixed
     */
    public function update(User $current_user, User $user)
    {
        // if the profile owner is the currently signed in user, return true.
        return $current_user->is($user);
    }

    /**
     * If the current user is not the user, yes they can follow.
     * @param  User   $current_user The current authenticated user.
     * @param  User   $user The users profile
     * @return Boolean True or false. 
     */
    public function follow(User $current_user, User $user)
    {
        // if the profile owner is not the currently signed in user, return true.
        return $current_user->isNot($user);
    }

    /**
     * User can reteweet the tweets of other users.
     * @param  User   $current_user The current authenticated user
     * @param  User   $user The owner. i/e profile/tweet etc
     * @return Boolean True or false.
     */
    public function retweet(User $current_user, User $user)
    {
        // if the profile owner is not the currently signed in user, return true.
        return $current_user->isNot($user);
    }

    /**
     * The user can like someone elses tweet but not their own.
     * @param  User   $current_user [description]
     * @param  User   $user         [description]
     * @return [type]               [description]
     */
    public function like_tweet(User $current_user, User $user)
    {
        // if the profile owner is not the currently signed in user, return true.
        return $current_user->isNot($user);
    }

    /**
     * The user can delete their own tweets
     * @param  User   $current_user The current authenticated user
     * @param  User   $user The owner. i/e profile/tweet etc
     * @return Boolean True or false.
     */
    public function delete_tweet(User $current_user, User $user)
    {
        // if the profile owner is not the currently signed in user, return true.
        return $current_user->is($user);
    }
}
