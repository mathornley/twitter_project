<?php

namespace App;
use App\Tweet;
use App\Message;

trait Messaging
{
	/**
     * The messages the user has sent to someone.
     * @return [type] [description]
     */
    public function sent_messages()
    {
        return $this->hasMany(Message::class,'user_id');
    }

    /**
     * The messages the user has received.
     * @return [type] [description]
     */
    public function received_messages()
    {
        return $this->hasMany(Message::class, 'message_user_id');
    }
}