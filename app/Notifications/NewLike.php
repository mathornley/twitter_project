<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewLike extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tweet)
    {
        // Get the tweet details liked. 
        $this->tweet = $tweet;
        // Get the current authenticated user - improves readability. 
        $this->likedBy = current_user();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hi, '.$this->tweet->user->username.'......You have a new like on Tweety')
                    ->line($this->likedBy->username.' liked your tweet: '.$this->tweet->text)
                    ->action(current_user()->username.'\'s profile', url('/profile/'.current_user()->username))
                    ->line('Thank you for using Tweety!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
                'message'=>current_user()->username.' liked your tweet: ' . $this->tweet->text
        ];
    }
}
