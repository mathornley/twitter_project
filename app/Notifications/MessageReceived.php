<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MessageReceived extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message, $user)
    {
        //ddd($message['message']);
        $this->to = $user;
        $this->message = $message['message'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Hi, '.$this->to->username.'! You have a new direct message')
                    ->greeting('Hi, '.$this->to->username.' You have a new direct message from '. current_user()->username)
                    ->line($this->message)
                    ->action(current_user()->username.'\'s profile', url('/profile/'.current_user()->username))
                    ->line('Thank you for using Tweety!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
                'message'=>current_user()->username.' has sent you a new direct message'
        ];
    }
}
