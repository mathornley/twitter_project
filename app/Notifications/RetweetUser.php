<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use App\User;

/**
 * A notification to let the owner of the tweet know they have had their tweet, retweeted.
 */
class RetweetUser extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($retweet, $user)
    {
        // The original tweet details. 
        $this->retweet = $retweet;

        // The original owner of the tweet.
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Hi, '.$this->user->username.'......')
                    ->greeting('Hi, '.$this->user->username.'......')
                    ->line(current_user()->username.' retweet your tweet at '.$this->retweet->created_at)
                    ->line($this->retweet->text)
                    ->action(current_user()->username.'\'s profile', url('/profile/'.current_user()->username))
                    ->line('Thank you for using Tweety!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
                'message'=>current_user()->username.' retweet your tweet',
                'tweet'=>$this->retweet->text,
        ];
    }
}
