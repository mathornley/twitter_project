<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// This is required to use Rule:: in validation.
use Illuminate\Validation\Rule;

use App\User;
use App\Tweet;

class ProfileController extends Controller
{
    
    /**
     * Display the users profile and pagenate the results.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
       return view('profiles.show', [
            'user'=>$user,
            'tweets'=>$user
                ->tweets()
                ->paginate(5)
        ]);
    }

    /**
     * Access the UserPolicy to see if this user has permission to edit this profile.
     * Show the form for editing the specified users profile.
     * Only show the form if the profile is that of the current user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // Could use and pass the error code // abort_if($user->isNot(Current_user()), 404);

        // Policy: Make sure this is the current user and their profile. This uses: UserPolicy & 'edit'. 
        $this->authorize('edit', $user);
        
        // Return the view with the update profile form. 
        return view('profiles.edit', compact('user'));   
    }


    /**
     * Update the users profile after validation
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        //dd(request('avatar'));

        // Make sure this user is allowed to update the profile. 
        $this->authorize('update', $user);

        // Validate the input
        $validation = request()->validate([
            "name"=>['required','string','max:255'],
            
            "username"=>[
                'required', 
                'string', 
                'max:255', 
                Rule::unique('users')->ignore($user)
            ],

            "location"=>['string','max:60'],
            
            "avatar"=>'file',

            "email"=>[
                'required',
                 'email', 
                 'string',
                 Rule::unique('users')->ignore($user)
             ],

            "bio"=>'max:255'//,

            // Make sure the password and password confirm match. 
            //"password"=>['required', 'string', 'confirm', 'min:8', 'max:255', 'confirmed']
        ]);

        // Add the avatar, if there is one, otherwise leave it.
        if(request('avatar'))
        {
            // Store avatar and then return the path. 
            $validation['avatar'] = request('avatar')->store('avatars');
            //dd($validation['avatar']);
        }   

         // Add the avatar, if there is one, otherwise leave it.
        if(request('banner'))
        {
            // Store avatar and then return the path. 
            $validation['banner'] = request('banner')->store('banners');
            //dd($validation['avatar']);
        }   
             
        // Finally save the record. 
        $user->update($validation);

        // Redirect the user to their profile page.
        return redirect(route('profile', $user));
    }
}
