<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use App\Like;

// New like notification
use App\Notifications\NewLike;

class LikesController extends Controller
{
    
    /**
     * Store a newly created like in storage.
     * Or unlike, if already liked. 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Tweet $tweet)
    {
        // Toggle to find out if this to be a like or unlike. 
        current_user()
            ->toggleLike($tweet);

        // Notify the user of a new like. 
        $tweet->user->notify(new NewLike($tweet));

        // Return back to the profile. 
        return back();
    }
}
