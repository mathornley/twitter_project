<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification; // Need this for the notifications if using facade.

//use Illuminate\Support\Facades\Mail; // Not using mail.
use App\Notifications\FollowUser;
use App\Mail\FollowTest;
use App\User;

use App\Events\FollowedUser;

class FollowsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * This finally uses the User model trait: Followable. 
     * @param  User $user This is the user to be followed. 
     * @return [boolean] This is the response from the model as to wether this is follow/unfolow.
     */
    public function store(User $user)
    {
        $type = current_user()
            ->toggleFollow($user); // This uses the User trait: Follawable.php

        // Notifications - let the new followed user know if they are being followed and by who
        if($type===true)
        {
            // This is better to notify if only sending to one person.
            $user->notify(new FollowUser($user));

            // Better if you are notifying a collection of users
            //Notification::send($user, new Following()($user));
            
            // Events for the new user added alert. 
            //FollowedUser::dispatch($user);
        }

        // Just the email option, won't be using this after all.
        /*Mail::to($user)
            ->send(new FollowTest($user));*/

        //resolve('followed_user')->alert(); // Resolve service out of the service container. 

        // Redirect back to the tweets page. 
        return back();
    }
}
