<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use App\Notifications\TweetAdded;

class TweetsController extends Controller
{
     /**
     * Show the main tweet page and all the tweets.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       return view('tweets.index', [
            'tweets'=>current_user()
              ->timeline()
        ]);
    }

    /**
     * Store a newly created resource in storage after validation.
    */
    public function store()
    {
       // Back end validate the tweet.
       $validated = $this->validateTweet();

       // Add and save the tweet.
       Tweet::create([
          'text'=> request('text'),
          'user_id'=>auth()->user()->id  
       ]);
 
       // Redirect to the tweets page which is called 'home'.
       return redirect()->route('home');
    }

    /**
     *  Some simple tweet validation
     * @return [array] [Returns array of validated tweet ]
     */
    public function validateTweet()
    {
        return request()->validate([
            'text'=>['required', 'max:255']
       ]);
    }

    /**
     * Remove the tweet and redirect the page back.
     *
     * @param  $tweet instance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tweet $tweet)
    {
        // Delete the tweet.
        $tweet->delete();

        // Redirect the page back to wherever it has come from. 
        return back();
    }
}
