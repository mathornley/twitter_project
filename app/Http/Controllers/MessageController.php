<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;

// Used to notify for messages
use App\Notifications\MessageReceived;

class MessageController extends Controller
{
	/**
	 * Get all the messages and the form to message.
     * 
	 */
    public function index()
    {
        // Paginate the received messages
        $received = auth()
            ->user()
            ->received_messages()
            ->latest()
            ->paginate(2);

        // Paginate the sent messages
        $sent = auth()
            ->user()
            ->sent_messages()
            ->latest()
            ->paginate(2);

    	// Return a view with the messages. 
    	return view('messages.index', [
            'received'=>$received,
            'sent'=>$sent],
        );
    }

    /**
     * Validate and save the message sent.
     * @return Retdirects back to the message view. 
     */
    public function store()
    {
    	// Validate the message. 
		request()->validate([
    		'user'=>'required',
    		'message'=>['required','string','max:255','min:10']
    	]);

  		// Store the message in the table. 
    	Message::create([
    		'user_id'=>auth()->user()->id,
    		'message_user_id'=>request('user'),
    		'message'=>request('message')
    	]);

        // Notify this user of a new message sent to them.
        $this->user = User::find(request('user'));

        // Notify the user they have a new message. Grab some details for the email later. 
        $this->user->notify(new MessageReceived(request()->all(), $this->user));

    	// Reirect to the messages view. 
    	return redirect()->route('messages');
    }

    /**
     * Remove the message.
     * @param  [string] $message This is the id of the message.
     */
    public function destroy($message)
    {
    	// Find and remove the message
    	Message::find($message)->delete(); // delete() on controller/ detach() on model. 

    	// Redirect to the messages view. 
    	return redirect()->route('messages');
    }
}
