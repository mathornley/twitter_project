<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ExploreController extends Controller
{
    /**
     * Display a listing of all users for the explore page.
     * Exclude the current signed in user. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('explore.index', [
            'users'=>User::whereNotIn('id', [current_user()->id])
                ->orderBy('username','asc')
                ->paginate(50),
        ]);
    }
}
