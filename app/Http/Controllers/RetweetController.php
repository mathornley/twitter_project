<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Notifications;
use App\Notifications\RetweetUser;


class RetweetController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * Get the tweet to retweet - this is in $tweet.
     * Get the current auth user who wants to retweet it. 
     * Save it to the table for the current user, with the retweet flag.
     * Direct back to the view you've come from
     *
     */
    public function store(Tweet $tweet)
    {
        // Add the retweet to the table for the current_user()
        Tweet::create([
            'user_id'=>current_user()->id, // Current auth user
            'text'=>$tweet['text'], // The tweet text
            'rt'=>1, // This is 1 for yes, a retweet.
            'tweet_id'=>$tweet['id'] // The id of row of the initial tweet by the user retweeted.        
        ]);

        // Get the person who owns the original tweet.
        $user = User::find($tweet->user_id);

        // Alert the user who owns the tweet they have been rewteeted and by who.
        $user->notify(new RetweetUser($tweet, $user));

        // Redirect the browser back. 
        return back();
    }
}
