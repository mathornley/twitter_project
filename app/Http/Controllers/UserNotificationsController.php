<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{
    /**
     *  Show all the users unread notifications.
     *  Once the unread notifications are fetched, marked them immediately as read so they won't reappear.
     * @return [obj] Collection of all the users unread notifications. 
     */
    public function show()
    {
    	// Get all the notifications. 
    	$notifications = current_user()->unreadNotifications;

    	// Mark the notifications as read.
    	$notifications->markAsRead();

    	// Send the notifications to the view. 
    	return view('notifications.index', [
    		'notifications'=>$notifications
    	]);
    }
}
