<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\MikeTest;
use \Notifications\TweetAdded;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Get a singleton imstance.
        $this->app->singleton('tweet_added', function ($app){
           return new TweetAdded;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
