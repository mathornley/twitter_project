<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;

// We need these for thr listener and event classes to be included. 
use App\Events\FollowedUser;
use App\Listeners\FollowedBy;

use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        // An event for a new user being followed by the authenticated user. 
        FollowedUser::class => [
            FollowedBy::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
