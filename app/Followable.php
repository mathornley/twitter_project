<?php

namespace App;
use App\Tweet;

trait Followable
{
	/**
     * To find out who the user follows.
     * We have to use the table name for 'follows', the key in 'user' and the foreign key for 'follows' table. 
     * @return [type] [description]
     */
    public function follows()
    {
        return $this->belongsToMany(User::class, 'follows', 'user_id', 'following_user_id')
        ->withTimestamps();
    }

    /**
     * Add to the users followers.
     * @param  User   $user The currently signed in user.
     */
    public function follow(User $user)
    {
        return $this->follows
            ->save($user)
                ->withTimestamps();
    }

    /**
     * Get the users followers
     * @return [type] [description]
     */
    public function getFollowers()
    {
        return $this->belongsToMany(User::class,'follows','following_user_id'); 
    }

    /**
     * Count the number of followers the user has. 
     * @return [type] [description]
     */
    public function getfollowersCountAttribute()
    {
        // Count the number of followers the user has. 
        return $this->getFollowers->count();
    }

    /**
     * Count the number of users the user follows. 
     * @return [type] [description]
     */
    
    public function getfollowingCountAttribute()
    {
        // Count the number of followers the user has. 
        return $this->follows->count();
    }

    /**
     * A toggle method to save and detach a follower
     * @param  $user [The user to follow or unfollow]
     * @return [obj]
     */
    public function toggleFollow($user)
    {
       // Already following the user, so unfollow
       if($this->follows->contains($user))
       {
            $this->follows()
                ->withTimestamps()
                ->detach($user);

            return false; // flag for unfollow for notifications
       }

       // Not following, so follow the user
       else
       {    
            $this->follows()
                ->withTimestamps()
                ->attach($user);

             return true; // flag for follow for notifications
       }
    }

    /**
     * See if the user passed in follows the current user.
     * @param  User $user [The profiled user]
     * @return [obj]
     */
    public function isFollowing(User $user)
    {   
        return $this->follows()
            ->where('following_user_id',$user->id)
                ->exists();
    }
}