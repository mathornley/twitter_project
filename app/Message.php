<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
	public $guarded = []; // Allow all, we'll sort on input.

    /**
     * 	Get the user instance of the message.
     * @return [type] [description]
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    /**
     * 	The username of the sender of the message. 
     * @return [type] [description]
     */
    public function getSenderAttribute()
    {
    	return $this->user->username;
    }

    /**
     * 	The username of the person the message is for. 
     * @return [type] [description]
     */
    public function getReceiverAttribute()
    {
    	return User::find($this->message_user_id)
    			->username;
    }
}