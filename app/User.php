<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Tweet;
use App\Like;

class User extends Authenticatable
{
    use Notifiable, Followable, Messaging, Tweeting, Liked;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','banner','location','password','username', 'bio', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Already hashed this in RegisterController.php under 'password'
    /*public function setPasswordAttribute($value)
    {
         $this->attributes['password'] = bycryp($value);
    }*/ 

    
    /**
     * Helper function to get the users avatar from the free API - test only and will go into DB eventually.
     * If there is no profile picture, the path to the default image is used.  
     * From the docs: Once a file has been stored and the symbolic link has been created, you can create a URL to the files using the asset helper:
     * Avatar is set to default 200px in size.
     * Email address currently ensures the avatar consistency.
     * @return [string] This is the path to the users profile picture. 
     */
    public function getAvatarAttribute($value)
    {
        return asset($value ? 'storage/' . $value : '/images/default.jpg');
    }

    /**
     * Get the banner image for the users profile or use the default. 
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getBannerAttribute($value)
    {
        return asset($value ? 'storage/' . $value : '/images/banner.jpg');
    }

    /**
     *  Get the path to the current user instance profile.
     *  Possibly, $append argument to extend the link - can used to extend URL : i.e 'edit' - so this  would be: "/profile/MikeThornley/edit" 
     *  @return [string] This is the users profile link, or can append and make the link with the /edit.
     */
    public function profile($append=null)
    {
        // Path to the profile.
        $path = route('profile', $this->username);

        // Check if this path should be just the profile link, or the edit link instead. 
        $path = $append ? "{$path}/{$append}/" : $path;

        return $path;
    }
    
    /**
     * Laravel <=6 - use this method, after v6, you can do this on the route itself. 
     * Using this so we can use a user name in the route, instead of the ID Laravel normally uses for route/model binding. Now you can use /profile/mikethornley 
     * This will still enable route/model binding to work and still find the user
     * @return [type] [description]
     */
    public function getRouteKeyName()
    {
        return 'username';
    }
}
