
<div class="border border-blue-400 rounded-lg py-6 p-8 mb-5">

    <form method="POST" action="/tweets/">

        @csrf

        <textarea name="text" id="" required rows="3" class="w-full" placeholder="What do you want to say in 255 characters or less?">{{old('text')}}</textarea>

        <hr class="my-4">

        <footer class="flex justify-between">

            <img class="mr-2 rounded-full" width="50" height="50" src="{{auth()->user()->avatar}}" alt=""/>

            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 ml-2 mt-4 px-4 rounded" 
            type="submit">
            Fire away!
            </button>
        </footer> 
    </form>
</div> 

@error('text')

<div class="mb-5 text-red-500 font-bold text-center">
        <p>{{$message}}</p>
</div>

@enderror
