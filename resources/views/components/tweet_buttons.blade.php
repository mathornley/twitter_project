<div class="flex justify-between items-left mt-10">

    <!-- Retweet this tweet of someone elses-->
    @can('retweet', $tweet->user)
     <div>                
        <form method="POST" action="{{route('retweet',$tweet)}}">
            
            @csrf 

            <p class="text-sm mt-3 ml-1 font-bold text-blue-400">
                <button type="submit">
                    <i class="fa fa-retweet" style="font-size:24px"></i>
                    Retweet
                </button>
                <span class="text-gray-700">
                    {{$tweet->isRetweeted($tweet) ? '('.$tweet->isRetweeted($tweet).')' : ''}}
                </span>
            </p>
        </form>
    </div>    
    @endcan
    <!-- Retweet this tweet of someone elses-->
            
    <!-- Delete your own tweet-->
    @can('delete_tweet', $tweet->user)
    <div>    
        <form method="POST" action="{{route('destroy_tweet', $tweet)}}">
        
            @csrf
            @method('DELETE')

            <p class="text-sm mt-3 ml-1 font-bold text-red-500">
                <button type="submit">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    Delete tweet
                </button>
            </p>
        </form>
    </div>    
    @endcan
    <!-- Delete your own tweet-->

    

    <!-- Allow clickable like -->
    @if($tweet->user->isNot(current_user()))
        <div>
           <p class="text-sm mt-3">
            <form method="POST" action="/likes/{{$tweet->id}}/">
                @csrf
                <button type="submit">
                    <i class="{{$tweet->likescount>0 ? 'text-green-500' : ''}} fa fa-thumbs-up" aria-hidden="true"></i>
                    {{$tweet->likescount>0 ? $tweet->likescount : '0'}}
                </button>
            </form>
           </p>
        </div>
        <!-- Allow clickable like -->

        <!-- Just display the likes as this is the author, don't allow clickable -->
        @else
        <div>
               <p class="text-sm mt-3">
                <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                ({{$tweet->likescount>0 ? $tweet->likescount : '0'}})
               </p>
        </div>
    @endif
    <!-- Just display the likes as this is the author, don't allow clickable -->    
</div>