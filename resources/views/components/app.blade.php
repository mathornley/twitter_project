<x-master>
    <section class='px-8'>   
        <main class=' container mx-auto'>
            <div class="lg:flex lg:justify-between">

                <div class="lg:w-32">

                    @include("_sidebar-links")
                
                </div>
                
                <div class="lg:flex-1 lg:mb-20 mx-10" style="width:700;">
                    
                    {{$slot}}

                </div>

                <div class="lg:w-1/6 bg-gray-100 border border-gray-300 rounded-lg p-4">
                    
                    @include("_friends-list")
                
                </div>
             </div>
        </main>
    </section>
</x-master>