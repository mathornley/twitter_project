
@can('follow', $user)

	<form method="POST" action="{{route('follow', $user)}}">
	
		@csrf
	        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 ml-2 mt-4 px-4 rounded" 
	        type="submit">
	        
	        {{ auth()->user()->isFollowing($user) ? 'Unfollow me' : 'Follow me' }}
	        
	        </button>
	</form>

@endcan
