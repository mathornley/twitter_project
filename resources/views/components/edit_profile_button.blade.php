
@can('edit',$user)

	<form method="get" action="{{$user->profile('edit')}}">

		@csrf
			<button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 ml-2 px-4 rounded" 
        	type="submit">
       			Edit profile
        	</button>
	</form>

@endcan
