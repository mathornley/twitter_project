 
<ul>
	<li>
		<a class="font-bold text-lg mb-4 block" href='/tweets/'>Home</a>
	</li>
	<li>
		<a class="font-bold text-lg mb-4 block" href='/explore/'>Explore</a>
	</li>
	<li>
		<a class="font-bold text-lg mb-4 block" target="_blank" href='http://mikedeveloper.com'>
		Developers website</a>
	</li>
	<li>
		<a class="font-bold text-lg mb-4 block" href="{{route('profile', auth()->user())}}">Profile</a>
	</li>
	<li>
		<a class="font-bold text-lg mb-4 block" href="/messages/">Messages
			@if(current_user()->unreadNotifications->where('type','App\Notifications\MessageReceived')->count())
			<span class="text-red-500">{{'('.current_user()->unreadNotifications->where('type','App\Notifications\MessageReceived')->count().')'}}</span>
			@endif
		</a>
	</li>

	<!-- Show notifications if there are any -->
	<li>
		<a class="font-bold text-lg mb-4 block" href="/notifications/user/">Notifications
			<span class="text-red-500">{{current_user()->unreadNotifications->count() ? "(".current_user()->unreadNotifications->count().")" : ""}}</span>
		</a>
	</li>
	<!-- Show notifications if there are any -->

	<!-- Logout button - a componet -->
	<li>
		<x-logout></x-logout>
	</li>
</ul>
