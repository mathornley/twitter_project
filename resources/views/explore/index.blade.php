<x-app>
	<h2 class="font-bold text-2xl">Explore page</h2>
	
	<div class="mt-5">
		@foreach($users AS $user)
		
			<div class="mt-5 flex justify-between">		
				<a title="{{$user->username}}" href="{{route('profile', $user)}}">
					<img class="rounded-full" src="{{$user->avatar}}" alt="{{$user->username}}'s avatar" width="60"/>
					<div>
						<p class="font-bold text-sm">{{'@'.$user->username}}</p>
						@if(current_user()->isFollowing($user))
							<p>
								<i class='fa fa-check' aria-hidden='true'></i>
								You already follow {{$user->username}}
							</p>
						@endif
					</div>
				</a>
			</div>
		
		@endforeach

		{{$users->links()}}
	</div>
</x-app>