

	@forelse($tweets as $tweet)
		<div class="border border-gray-300 rounded-lg">		
			@include("_tweet")
		</div>
		
	@empty
		<div>
			<p>This user hasn't tweeted anything yet.</p>
		</div>	
	@endforelse

	{{$tweets->links()}}
