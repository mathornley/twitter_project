

<div class="flex relative p-4 {{ $loop->last ? '' : 'border-b border-b-gray-400'}}">

    <div class="mr-2 flex-shrink-0">
        <a href="{{$tweet->user->profile()}}">
            <img class="mr-2 rounded-full" width="50" height="50" src="{{$tweet->user->avatar}}" alt=""/>
        </a>
    </div>

    <div class="w-full">

            <!-- This is a standard tweet -->
            @if(!$tweet['rt'] && !$tweet['tweet_id'])
                <h5 class="font-bold mb-4">
                    <a href="{{$tweet->user->profile()}}">{{$tweet->user->name}}</a>
                </h5> 
             <!-- This is a standard tweet -->

            <!-- This is a retweet -->
            @else
                <h5 class="font-bold mb-4 bg-green-300 p-2 text-white rounded">
                    <a href="{{$tweet->user->profile()}}">
                        {{'@'.$tweet->user->username}} retweeted:
                    </a> 
                    <a href="{{route('profile',$tweet->username)}}">
                        {{'@'.$tweet->username}}
                    </a>
                    <i class="fa fa-retweet" style="font-size:24px"></i>
                </h5> 
            @endif
            <!-- This is a retweet -->
        
        <p class="{{($tweet['rt'] && $tweet['tweet_id']) ? 'text-gray-600 italic ml-10 ' : 'text-gray-600'}}">
            {{ $tweet->text }}
        </p>
       
        <!-- This is a tweet -->
        @if(!$tweet['rt'] && $tweet['id'])
            <p class="text-sm mt-3">
                Tweeted: {{\Carbon\Carbon::parse($tweet->updated_at)->diffForHumans()}}
            </p>
        <!-- This is a tweet -->
       
        <!-- This is a retweet -->
        @else
            <p class="text-sm mt-1 italic ml-10 ">
                <em>Retweeted: {{\Carbon\Carbon::parse($tweet->updated_at)->diffForHumans()}}</em>
            </p>
        @endif
        <!-- This is a retweet -->

        <!-- The tweet buttons are store here - retweet & liked -->
        <x-tweet_buttons :tweet="$tweet"></x-tweet_buttons>
        <!-- The tweet buttons are store here - retweet & liked -->
    </div>
</div>