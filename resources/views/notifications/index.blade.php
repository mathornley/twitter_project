<x-app>

<h2 class="mb-5 font-bold text-xl">Notifications for: {{ucfirst(current_user()->username)}}</h2>

	<div>

		<!-- There are currently no new notifications -->
		@if($notifications->isEmpty())
			<p> There are currently no new notifications.</p>
		@endif

		<!-- Iterate the notifications -->
		@foreach($notifications AS $notification)

			<!-- Notification for someone retweeting the user -->
			@if($notification->type ==  "App\Notifications\RetweetUser")
				<div>
					<div class="mt-5 mb-5">
						{{\Carbon\Carbon::parse($notification->created_at)->format('d/m H:i')}}  
						<strong>{{$notification->data['message']}}</strong>:
						{{$notification->data['tweet']}}
					</div>	
				</div>
			@endif
	
			<!-- Notification for a follow to the user -->
			@if($notification->type ==  "App\Notifications\FollowUser")
				<div>
					<div class="mt-5 mb-5">
						{{\Carbon\Carbon::parse($notification->created_at)->format('d/m H:i')}}  
						<strong>{{$notification->data['message']}}</strong>
					</div>	
				</div>
			@endif

			<!-- Notification for a tweet like to the user -->
			@if($notification->type ==  "App\Notifications\NewLike")
				<div>
					<div class="mt-5 mb-5">
						{{\Carbon\Carbon::parse($notification->created_at)->format('d/m H:i')}}  
						<strong>{{$notification->data['message']}}</strong>
					</div>	
				</div>
			@endif

			<!-- Notification for a message sent to the user -->
			@if($notification->type ==  "App\Notifications\MessageReceived")
				<div>
					<div class="mt-5 mb-5">
						{{\Carbon\Carbon::parse($notification->created_at)->format('d/m H:i')}}  
						<strong>{{$notification->data['message']}}</strong>
					</div>	
				</div>
			@endif

		@endforeach
	</div>

<!-- End iterate the notifications -->
</x-app>