<x-app>

<h2 class="font-bold text-2xl mb-4">Messages for: {{ucfirst(current_user()->username)}}</h2>

	<h3 class="font-bold text-xl mb-5">
		Send a new message:
	</h3>


<div class="border border-blue-400 rounded-lg p-6 mb-5">

    <form method="POST" action="/messages/">

        @csrf

		<div class="mt-5 mb-5 border border-gray-100">
			<textarea class="w-full p-7" rows="5" name="message" id="message" placeholder="What do you want to say?" required>{{old('message')}}</textarea>
		</div>

		<div>

		    <select name="user" mb-4 required class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">

			  <option value="">Please select a user</option>

			  <!-- Only allow the user to message someone that follows them -->
		      @foreach(current_user()->getFollowers AS $follows)
		      	<option value="{{$follows['id']}}">
		      		{{$follows['name']}} | {{$follows['username']}}
		      	</option>
		      @endforeach
		    
		    </select>

    	</div>

    	@error('message')
			<p class="text-red-500 font-bold mt-5 mb-5">{{$message}}</p>
		@enderror

		@error('user')
			<p class="text-red-500 font-bold mt-5 mb-5">{{$message}}</p>
		@enderror

        <hr class="my-4">

        <footer class="flex justify-between">

            <img class="mr-2 rounded-full" width="50" height="50" src="{{auth()->user()->avatar}}" alt=""/>

            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 ml-2 mt-4 px-4 rounded" 
            type="submit">
            Send message
            </button>
        </footer> 
    </form>
</div> 

@error('text')

<div class="mb-5 text-red-500 font-bold text-center">
        <p>{{$message}}</p>
</div>

@enderror


	<h3 class="font-bold text-xl mb-5">
		Received messages
	</h3>

	@forelse($received AS $message)
	
		<div class="border border-gray-300 rounded-lg p-5 border-blue-200">	
		
			<div class="mb-4">
				
					<p class="mb-5 font-bold">Message sender: {{$message->sender}}</p>
					<p>{{$message['message']}}</p>

					<hr class="mt-3"/>
					
					<div class="mt-3 flex justify-between">
						<p>{{$message['created_at']->diffForHumans()}} on </p>
						<p>{{$message['created_at']->format('d/m/Y H:i\h\r\s')}}</p>
						
						
						<form method="POST" action="/message/{{$message->id}}/delete/">
							
							@csrf
							@method('delete')

							<i class="fa fa-trash" aria-hidden="true"></i>
							<button>
								<p class="text-red-900"> Delete message</p>
							</button>
						</form>
					</div>
			</div>
		</div>
		<br/>
		@empty
			
		<p>You haven't received any messages.</p>
	
	@endforelse

	{{ $received->links() }}

	<hr class="mb-5 mt-5"/>


	<h3 class="font-bold text-xl mb-5">
		Sent messages
	</h3>

	@forelse($sent AS $message)
	
		<div class="border border-gray-300 rounded-lg p-5 border-blue-200">	
		
			<div class="mb-4">
				
					<p class="mb-5 font-bold">Message sent to: {{$message->receiver}}</p>
					<p>{{$message['message']}}</p>

					<hr class="mt-3"/>
					
					<div class="mt-3 flex justify-between">
						<p>{{$message['created_at']->diffForHumans()}} on </p>
						<p>{{$message['created_at']->format('d/m/Y H:i\h\r\s')}}</p>
						
						<form method="POST" action="/message/{{$message->id}}/delete/">
							
							@csrf
							@method('delete')

							<i class="fa fa-trash" aria-hidden="true"></i>
							<button>
								<p class="text-red-900"> Delete message</p>
							</button>
						</form>
					</div>
			</div>
		</div>
		<br/>
		@empty
			
		<p>You haven't received any messages.</p>
	
	@endforelse

	{{ $sent->links() }}

</x-app>