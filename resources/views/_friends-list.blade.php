
<h3 class="font-bold text-xl mb-4">Following</h3>

<ul>
	
	@forelse(current_user()->follows AS $friend)
	
		<li class="{{$loop->last ? '' : 'mb-4'}}">
			<div>
				<a class="flex items-center text-sm" href='{{route("profile",$friend)}}'>
					<img src="{{$friend->avatar}}" class="mr-2 rounded-full h-10 w-10" alt=""/>{{$friend->name}}
				</a>
			</div>
		</li>
	
	@empty
		
		<li>You aren't following anyone</li>
	
	@endforelse
</ul>
