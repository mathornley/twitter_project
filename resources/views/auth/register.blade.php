<x-master>

<div class="container mx-auto flex justify-center">
    
    <div class="w-4/12 px-6 py-4 bg-gray-400 rounded-lg">
        
                <div class="font-bold text-lg mb-5">{{ __('Register') }} for Tweety</div>

                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                             <div class="mb-1">
                                <label for="username" class="block text-gray-700 text-md font-bold mb-2">Username</label>

                                <div class="col-md-6">
                                    <input id="username" type="text" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="name" autofocus>

                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="name" class="block text-gray-700 text-md font-bold mb-2">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                             <div class="mb-1">
                                <label for="email" class="block text-gray-700 text-md font-bold mb-2">{{ __('E-Mail Address') }}</label>

                                <div>
                                    <input id="email" type="email" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-1">
                                <label for="email" class="block text-gray-700 text-md font-bold mb-2">{{ __('Location') }}</label>

                                <div>
                                    <input id="location" type="text" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('location') is-invalid @enderror" name="location" placeholder="i.e New York, USA" value="{{ old('location') }}" autocomplete="location">

                                    @error('location')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-1">
                                <label for="password" class="block text-gray-700 text-md font-bold mb-2">{{ __('Password') }}</label>

                                <div>
                                    <input id="password" type="password" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="mb-1">
                                <label for="password-confirm" class="block text-gray-700 text-md font-bold mb-2">{{ __('Confirm Password') }}</label>

                                <div>
                                    <input id="password-confirm" type="password" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="mb-1">
                                <div>
                                    <button type="submit" class="mb-2 bg-blue-500 rounded hover:bg-blue-600 shadow py-2 px-5 text-white text-xs">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>

                            <div class="mt-3 mt-3">
                                <div>
                                    Already registered? Sign in <a href="{{route('login')}}">here</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
</x-master>s