<x-master>

<div class="container mx-auto flex justify-center">
    
    <div class="w-4/12 px-6 py-4 bg-gray-400 rounded-lg">

        <div class="font-bold text-lg mb-5">{{ __('Login') }} to Tweety</div>

        <form method="POST" action="{{ route('login') }}">
        
        @csrf

            <div class="mb-2">

                <label for="email" class="block text-gray-700 text-md font-bold mb-2">
                    {{ __('E-Mail Address') }}
                </label>

                <input id="email" type="email" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus autocomplete>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            
            </div>


            <div class="mb-2">

                <label for="password" class="block text-gray-700 text-md font-bold mb-2">
                    {{ __('Password') }}
                </label>

                <input id="password" type="password" class="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                @enderror
            
            </div>

            <div class="mb-2">
                <button class="mb-2 bg-blue-500 rounded hover:bg-blue-600 shadow py-2 px-5 text-white text-xs" type="submit" class="btn btn-primary">
                {{ __('Login') }}
                </button>
            </div>

            <!-- Remeber me tickbox -->
            <div class="mb-4">
                <div class="flex justify-between-center">

                    <label class="block text-gray-700 text-md font-bold mb-2" for="remember">
                    {{ __('Remember Me') }}
                    </label>

                    <input class="ml-2 mt-2 form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                </div>

                <!-- Forgotten password link -->
                <div>

                    @if (Route::has('password.request'))
                        <p>
                            <em><a href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                            </a></em>
                        </p>
                    @endif
                
                </div>

                <!-- They're not registered - provide a link -->
                <div class="mt-3">
                    Not registered? Register <a href="{{route('register')}}">here</a>
                </div>
            </div>
        </form>
    </div>
</div>
</x-master>