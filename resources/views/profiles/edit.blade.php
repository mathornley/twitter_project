<x-app>

	<form class="w-full" method="POST" 
	action="{{route('update_profile',$user->username)}}" 
	enctype="multipart/form-data">
		
		@method('PATCH')
		@csrf
	
		<h2 class="text-xl mb-2 py-4 font-bold">Edit profile for: {{ucfirst($user->username)}}</h2> 
		<p class="text-sm mb-5 mt-0">You can update your profile details below.</p>

		<div class="flex flex-wrap -mx-3 mb-6">

			<div class="w-full px-3 mb-6 md:mb-0">
					
					<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-avatar">Profile banner</label>
						
					<div class="flex">
						<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-banner" type="file" name="banner"/>
					
						<img class="ml-3 mt-0" width="500" src="{{$user->banner}}" alt="{{$user->name}}'s banner"/>
					</div>

					@error('banner')
						<p class="font-bold text-red-700 mb-4">{{$message}}</p>
					@enderror

			</div>
		

			<div class="w-full px-3 mb-6 md:mb-0">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-name">Name</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="name" type="text" placeholder="Mike Smith.." name="name" value="{{$user->name}}"/>

				@error('name')
					<p class="font-bold text-red-700 mb-4">{{$message}}</p>
				@enderror

			</div>

		
			<div class="w-full px-3 mb-6 md:mb-0">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-username">Username</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-username" type="text" placeholder="MikeSmith1..." name="username" value="{{$user->username}}"/>
			
				@error('username')
					<p class="font-bold text-red-700 mb-4">{{$message}}</p>
				@enderror

			</div>
			
				<div class="w-full px-3 mb-6 md:mb-0">
					
					<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-avatar">Avatar</label>
						
					<div class="flex">
						<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-username" type="file" name="avatar"/>
					
						<img class="ml-3 mt-0" width="60" src="{{$user->avatar}}" alt="{{$user->name}}'s avatar"/>
					</div>

					@error('avatar')
						<p class="font-bold text-red-700 mb-4">{{$message}}</p>
					@enderror

				</div>
		

			<div class="w-full px-3 mb-6 md:mb-0">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-name">Your bio</label>
				<textarea name="bio" rows="8" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="bio" placeholder="Your bio, a chance to sell yourself, show off, tell your followers something about yourself...">{{$user->bio}}</textarea>

				@error('bio')
					<p class="font-bold text-red-700 mb-4">{{$message}}</p>
				@enderror


			</div>

			
			<div class="w-full px-3 mb-6 md:mb-0">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-username">Email</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-username" type="email" placeholder="MikeSmith@yahoo.com..." name="email" value="{{$user->email}}"/>
			
				@error('email')
					<p class="font-bold text-red-700 mb-4">{{$message}}</p>
				@enderror

			</div>

			<div class="w-full px-3 mb-6 md:mb-0">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-username">Location</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-location" type="text" placeholder="New York, USA" name="location" value="{{$user->location}}"/>
			
				@error('location')
					<p class="font-bold text-red-700 mb-4">{{$message}}</p>
				@enderror

			</div>
		
			<div class="w-full px-3">
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">Password</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="password" name="password"/>
				
				<p class="mb-3 text-red-600 text-xs italic">Make it as long and as crazy as you'd like</p>
				
			</div>

		
			<div class="w-full px-3"> {{-- This HAS to be called "password_confirmation" --}}
				<label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">Password Confirmation</label>
				<input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password_confirmation" type="password" name="password_confirmation"/>
			
			</div>
	
			<button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 ml-2 mt-4 px-4 rounded">
				Update details
			</button>

			<button href="{{route('profile',$user)}}" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 ml-2 mt-4 px-4 rounded">
				Cancel
			</button>
			
		</div>
	</form>
</x-app>