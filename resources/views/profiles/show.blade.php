<x-app>

<header class="mb-6 relative">

		<div class="relative">

			<img class="mb-2 rounded h-50" style="width:100%;height:350px;" src="{{$user->banner}}" alt="Default profile banner"/>

			 <img 
			 class="mr-2 rounded-full h-50 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2"
			 style="width:150px;left:50%;"
			 src="{{$user->avatar}}" alt="The profile pic"/>
		
		</div>
	
		<div class="flex justify-between items-center mt-5">
			<div>
				<h2 style="max-width:300px;" class="font-bold text-2xl">{{$user->name}}</h2>
				<p class="text-lg text-lg text-gray-500">{{'@'.$user->username}}</p>
				<p class="text-md">{{$user->location ? $user->location : 'Tweetsville, USA'}}</p>
				<p class="mt-3 text-sm">Joined: {{$user->created_at->diffForHumans()}}</p>
			</div>

			<div class="flex justify-between items-center">


				{{-- BUTTONS: These are called: anonymous blade components --}}

				<x-edit_profile_button :user="$user"></x-edit_profile_button>
				
				<x-follow-button :user="$user"></x-follow-button>
				
				{{-- These are called: anonymous blade components --}}

	        </div>

		</div>

		<div class="flex justify-between items-left mt-10">
			
			<!-- Total TWeets count -->
			<p class="text-lg">Tweets: {{$user->tweetscount}}</p>
			<!-- Total Retweets count -->
			<p class="text-lg">Retweets: {{$user->retweetscount}} </p>
			<!-- The total number of likes of tweets by the user - all tweets -->
			<p class="text-lg">Likes: {{$user->userlikes}} </p>
			<!-- Total Followers count -->
			<p class="text-lg">Followers: {{$user->followerscount}}</p>
			<!-- Total Follows count-->
			<p class="text-lg">Following: {{$user->followingcount}}</p>
			
		</div>
					
		<div class="flex mt-3 py-3">
			{{$user->bio ? $user->bio : 'I am new to the tweetsphere!'}}
		</div>

</header>

		@include('_timeline',[
			'tweets'=>$tweets
		])

</x-app>