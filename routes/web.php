<?php

// Snippet for seeing all the database queries. 
//DB::listen(function($query){var_dump($query->sql, $query->bindings);});
//auth()->loginUsingId(6); // This is to spoof the user being logged in and used for testing

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home view not using auth, no sign-in required, splash page. 
Route::get('/', function () {
    return view('welcome');
});

// These routes require and use auth middleware and require a sign-in.
Route::middleware('auth')->group(function(){
	
	// TWeets main page, all tweets.
	Route::get('/tweets/', 'TweetsController@index')
		->name('home');
	
	// Store a new tweet
	Route::post('/tweets/', 'TweetsController@store');

	// Delete a tweet
	Route::delete('/tweets/{tweet}/delete/','TweetsController@destroy')
		->name('destroy_tweet');

	// Retweet someone elses tweet
	Route::post('/tweets/{tweet}/retweet/','RetweetController@store')
		->name('retweet');

	// Messages area - show all the messages.
	Route::get('/messages/','MessageController@index')
		->name('messages');

	// Send a message to a user
	Route::post('/messages/','MessageController@store');

	// Delete a message
	Route::delete('/message/{message}/delete/','MessageController@destroy')
		->name('destroy_message');
	
	// Explore the profiles of other users on Tweety. 
	Route::get('/explore/', 'ExploreController@index')
		->name('explore');

	// Likes - the user likes anothers post
	Route::post('/likes/{tweet}/','LikesController@store')
		->name('likes_tweet');
		
	// Profile area, view and update

	// Use the 'name' attribute in route/model binding instead of the primary key = user:name 
	Route::get('/profile/{user:username}/', 'ProfileController@show')
		->name('profile');
	
	// Show form to edit the profile
	Route::get('/profile/{user:username}/edit','ProfileController@edit')
		->name('edit_profile')
		->middleware('can:edit,user'); // Don't forget the wildcard.

	// Finally, update the users profile information. 	
	Route::patch('/profile/{user:username}','ProfileController@update')
		->name('update_profile');

	// Handles the follow/unfollow on a toggle method.
	Route::post('/profile/{user:username}/follow/','FollowsController@store')
		->name('follow');

	// Notifications start here.
	Route::get('/notifications/user/', 'UserNotificationsController@show')
		->name('notifications');
	
	// Logs you out & directs to the homepage.
	Route::post('/logout', 'LoginController@logout');
});

Auth::routes();


