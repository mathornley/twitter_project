<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    return [
        'user_id' => factory(\App\User::class),
        'message_user_id' => factory(\App\User::class),
        'message' => $faker->sentence,
    ];
});
