<?php

use Illuminate\Database\Seeder;
use App\User;


class InitialSetup extends Seeder
{
    /**
     * Run the database seeds for the quick intial set up.
     * Runs a factory to create tweets per user.
     *
     * @return void
     */
    public function run()
    {
    	$users = factory(App\User::class, 5)->create()->each(function($users){ // Create 5 users
            factory(App\Tweet::class, 10)->create(['user_id'=>$users->id]); // 10 tweets per user.
        });
    }
}
