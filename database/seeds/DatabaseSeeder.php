<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's databases.
     * This creates users, tweets and messages. 
     *
     * @return void
     */
    public function run()
    {
    	// run the main seeder class from here. 
        $this->call(InitialSetup::class);
    }
}
